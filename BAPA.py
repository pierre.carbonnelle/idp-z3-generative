
import glob
import re
from subprocess import check_output
from timeit import timeit

from utils import (problem, get_content, run,
        monotype1, monotype2, type1, type2,
        l_monotype1, l_monotype2, l_type1, l_type2, TIME_OUT)


out = ""

list = [f"./theories/BAPA/concrete.idp"]

print(f"Running {list[0]}")

for f in list:
        kernels=[
            lambda : l_monotype1(f, problem(f, "")),
            lambda : l_monotype2(f, problem(f, "")),
            lambda : l_type1(f, problem(f, "")),
            lambda : l_type2(f, problem(f, "")),
            lambda : monotype1(f, problem(f, "")),
            lambda : monotype2(f, problem(f, "")),
            lambda : type1(f, problem(f, "")),
            lambda : type2(f, problem(f, "")),
            lambda : "~",
            lambda : "~",
        ]
        labels=["m1", "m2", "t1", "t2",
                "lm1", "lm2", "lt1", "lt2"]

        times = []
        for i, k in enumerate(kernels):
            time = f"{k()}"
            if i < len(labels):
                print(labels[i], time, " s")
            times.append(time)

        minT = min(float(t) if "T" not in t and "~" not in t else 99999 for t in times)

        out += (f'BAPA & '
                + " & ".join(t if "T" in t or "~" in t or float(t) != minT else f"\\textbf{{{minT}}}" for t in times)
                # + f" & {expansion}"
                +" \\\\ \n")
        print(out)

Done = True