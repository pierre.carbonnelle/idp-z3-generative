import cpmpy
from cpmpy.solvers import CPM_minizinc
from cpmpy.tools import mus
import time

from .racks import RacksExample
from .racks_lifted import LiftedRacksExample
from .racks_model import RackConfiguration



x = input("Number of elements of type A (between 1 and 5)? ")

A = int(x)
B = 0
C = 0
D = 0

TIMEOUT = 200

start_time = time.time()
### lifted

n, Done = 1, False
while not Done and time.time() - start_time < TIMEOUT:

    lr: LiftedRacksExample = LiftedRacksExample(n,100)
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTA] == A
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTB] == B
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTC] == C
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTD] == D
    lr.timelimit = TIMEOUT
    solver = lr
    # slower: solver: CPM_minizinc = cpmpy.SolverLookup.get("minizinc", lr.model)
    if solver.solve():
        configuration = lr.build_from_solution()
        print(configuration.info())
        Done = True
    else:
        #print("unsatisfiable")
        # print(mus(lr.model.constraints))
        n += 1

lifted = round(time.time()-start_time, 3)
lifted_n = n
print(f"\n> Solved by lifted method in {lifted} sec ({lifted_n} lifted objects)")

### concrete

start_time = time.time()
n, Done = 1, False
while not Done and time.time() - start_time < TIMEOUT:

    print(n)
    lr: RacksExample = RacksExample(n)
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTA] == A
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTB] == B
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTC] == C
    lr.model += lr.OBJECTCOUNT[RackConfiguration.ELEMENTD] == D
    lr.timelimit = TIMEOUT
    solver = lr
    # slower: solver: CPM_minizinc = cpmpy.SolverLookup.get("minizinc", lr.model)
    if solver.solve():
        configuration = lr.build_from_solution()
        print(configuration.info())
        Done = True
    else:
        n += 1

concrete = round(time.time()-start_time, 3)


print(f"\n> Solved by lifted method in {lifted} sec ({lifted_n} lifted objects) ")
print(f"and in concrete domain in {concrete} sec ({n} concrete objects)")

Done = True