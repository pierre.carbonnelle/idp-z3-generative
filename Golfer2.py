""""
    Beware: need to change 'latin-1' by 'utf-8' in z3core.py !!!!!!!!!!!!!!!!!
"""
from math import lcm

from IDP_Z3.perfplot import bench
from utils import (get_content,set_content)
from IDP_Z3.idp_engine import IDP, Theory


GROUPSIZE = 4
TIME_OUT = 1000

#concrete = get_content("./theories/Golfer/concrete.idp")
lifted = get_content("./theories/Golfer2/lifted.idp")

def instantiate(code, p, w):
    th = (code.replace("$p$", str(p))
                .replace("$w$", str(w))
                .replace("$gr$", str(int(p/GROUPSIZE)))
                )
    kb = IDP.from_str(th)
    T = kb.get_blocks("T")[0]
    return T

# def expand(T):
#     model = next(Theory(T).expand(timeout_seconds=TIME_OUT, max=1))
#     out = str(model)
#     set_content("./theories/Golfer/concrete.out", str(out))

def generate(T, p, w):
    interp, out = Theory(T).generate(timeout_seconds=120,
                                     guess={'G_T':4, 'GR_T': 1},
                                     increment={'G_T':4, 'GR_T': 1},
                                     max={'G_T':p, 'GR_T': int(p/GROUPSIZE)},
                                     breaking=1, use_integers=True)
    set_content(f"./theories/Golfer2/lifted-{p}-{w}.out", str(out))

out = bench(
    setup=lambda n: (n,
                     instantiate(lifted, n, 1),
                     instantiate(lifted, n, 2),
                     instantiate(lifted, n, 3)),
    kernels=[
        lambda p, l1, l2, l3: generate(l1, p, 1),
        lambda p, l1, l2, l3: generate(l2, p, 2),
        lambda p, l1, l2, l3: generate(l3, p, 3),
    ],
    labels=["1 week", "2 weeks", "3 weeks"],
    n_range=[4*n for n in range(4,12)],
    xlabel="Golfers",
    # More optional arguments with their default values:
    # logx="auto",  # set to True or False to force scaling
    #logy=False,
    equality_check=None, #np.allclose,  # set to None to disable "correctness" assertion
    show_progress=True,
    # target_time_per_measurement=1.0,
    max_time=TIME_OUT,  # maximum time per measurement
    # time_unit="s",  # set to one of ("auto", "s", "ms", "us", or "ns") to force plot units
    # relative_to=1,  # plot the timings relative to one of the measurements
    # flops=lambda n: 3*n,  # FLOPS plots
    title=""
)
out.show(logy=True)
out.save("Golfer.pgf", transparent=True, bbox_inches="tight")

Done = True