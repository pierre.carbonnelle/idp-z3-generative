""""
    Beware: need to change 'latin-1' by 'utf-8' in z3core.py !!!!!!!!!!!!!!!!!
"""

from utils import (get_content,set_content, TIME_OUT)
from IDP_Z3.idp_engine import IDP, Theory
from IDP_Z3.perfplot import bench

N = 2  # pigeons per hole

concrete = get_content("./theories/Pigeon/concrete.idp")
lifted = get_content("./theories/Pigeon/lifted1.idp")
lifted2 = get_content("./theories/Pigeon/lifted2.idp")

def instantiate(code, p):
    th = (code.replace("$p$", str(p*N))
              .replace("$h$", str(int(p)))
              .replace("$n$", str(N))
         )
    if "hosts" in code:
        set_content("./theories/Pigeon/lifted2.lidp", th)
    elif "Theory(T).generate" in code:
        set_content("./theories/Pigeon/lifted1.lidp", th)
    kb = IDP.from_str(th)
    T = kb.get_blocks("T")[0]
    return T

def expand(T):
    model = next(Theory(T).expand(timeout_seconds=10, max=1))
    out = str(model)
    set_content("./theories/Pigeon/concrete.out", str(out))

def generate(T):
    interp, out = Theory(T).generate(timeout_seconds=10)
    set_content("./theories/Pigeon/lifted1.out", str(out))

def generate2(T):
    interp, out = Theory(T).generate(timeout_seconds=10)
    set_content("./theories/Pigeon/lifted2.out", str(out))

out = bench(
    setup=lambda n: (instantiate(concrete,n),
                     instantiate(lifted, n),
                     instantiate(lifted2, n)),
    kernels=[
        lambda p, l1, l2: expand(p),
        lambda p, l1, l2: generate(l1),
        lambda p, l1, l2: generate2(l2),
    ],
    labels=["concrete", "lifted-f", "lifted-p"],
    n_range=[n for n in range(1, 50)],
    xlabel="Pigeons",
    # More optional arguments with their default values:
    # logx="auto",  # set to True or False to force scaling
    #logy=False,
    equality_check=None, #np.allclose,  # set to None to disable "correctness" assertion
    show_progress=True,
    # target_time_per_measurement=1.0,
    max_time=10,  # maximum time per measurement
    # time_unit="s",  # set to one of ("auto", "s", "ms", "us", or "ns") to force plot units
    # relative_to=1,  # plot the timings relative to one of the measurements
    # flops=lambda n: 3*n,  # FLOPS plots
    title=""
)
out.show(logy=True)
out.save("Pigeon.pgf", transparent=True, bbox_inches="tight")

Done = True