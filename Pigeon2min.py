""""
    Beware: need to change 'latin-1' by 'utf-8' in z3core.py !!!!!!!!!!!!!!!!!
"""
from math import lcm

from IDP_Z3.perfplot import bench
from utils import (get_content,set_content)
from IDP_Z3.idp_engine import IDP, Theory

N = 2
M = 2
TIME_OUT = 200

concrete = get_content("./theories/Pigeon2min/concrete.idp")
lifted = get_content("./theories/Pigeon2min/lifted.idp")

def instantiate(code, pn):
    th = (code.replace("$p$", str(pn))
                .replace("$h$", str(int(pn*M//N)))
                .replace("$n$", str(N))
                .replace("$m$", str(M))
                )
    kb = IDP.from_str(th)
    T = kb.get_blocks("T")[0]
    return T

def expand(T):
    _, model = Theory(T).generate(timeout_seconds=TIME_OUT)
    out = str(model)
    set_content("./theories/Pigeon2min/concrete.out", str(out))

def generate(T):
    interp, out = Theory(T).generate(timeout_seconds=TIME_OUT)
    set_content("./theories/Pigeon2min/lifted1.out", str(out))

out = bench(
    setup=lambda n: (instantiate(concrete,n), instantiate(lifted, n)),
    kernels=[
        lambda p, _: expand(p),
        lambda _, p: generate(p),
    ],
    labels=["concrete", "lifted"],
    n_range=[n for n in range(N, 50)],
    xlabel="Pigeons",
    # More optional arguments with their default values:
    # logx="auto",  # set to True or False to force scaling
    #logy=False,
    equality_check=None, #np.allclose,  # set to None to disable "correctness" assertion
    show_progress=True,
    # target_time_per_measurement=1.0,
    max_time=TIME_OUT,  # maximum time per measurement
    # time_unit="s",  # set to one of ("auto", "s", "ms", "us", or "ns") to force plot units
    # relative_to=1,  # plot the timings relative to one of the measurements
    # flops=lambda n: 3*n,  # FLOPS plots
    title=""
)
out.show(logy=True)
out.save("Pigeon2min.pgf", transparent=True, bbox_inches="tight")

Done = True