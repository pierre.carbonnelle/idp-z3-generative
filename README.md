This repository contains the source code for the paper
"Using Symmetries to Lift Satisfiability Checking".
A separate pdf file contains Appendices for the paper.

Folders
-------

The FO(.) formulation of configuration problems are in the `theories` folder.
Each folder contain a `concrete.idp` file with the formulation of the problem in FO(.).
The 8 transformed formulations for use with iterative methods have the `.idp` extension.
The output obtained by running them has the `.out`extension.

The modified version of IDP-Z3 that we use is in the `IDP_Z3` directory.

The test program is test.py.

Installation
------------

To install and run the test programs:

* Install [python3](https://www.python.org/downloads/).
* Install [poetry](https://python-poetry.org/docs/#installation):
    * after that, logout and login if requested, to update `$PATH`

Open a terminal in this directory, then:

> poetry install
> poetry run python3 test.py

And respond to the prompt.
To run a particular file, enter 3 at the prompt.

If an error is raised in z3core.py:
change 2 occurrences of 'latin-1' to 'utf-8' in that file.
