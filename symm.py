""""
    Beware: need to change 'latin-1' by 'utf-8' in z3core.py !!!!!!!!!!!!!!!!!
"""

from utils import (get_content,set_content, TIME_OUT)
from IDP_Z3.idp_engine import IDP, Theory
from IDP_Z3.perfplot import bench

N = 2  # pigeons per hole
TIME_OUT = 20

standard = get_content("./theories/Rack_ABCD_symm/ABCD_n.idp")

def instantiate(code, n):
    th = (code.replace("$n$", str(n//4))
         )
    kb = IDP.from_str(th)
    T = kb.get_blocks("T")[0]
    return T

def generate(T, breaking):
    interp, out = Theory(T).generate(factor=1.5, timeout_seconds=TIME_OUT, breaking=breaking)
    set_content("./theories/Pigeon/lifted1.out", str(out))

out = bench(
    setup=lambda n: instantiate(standard,n),
    kernels=[
        lambda p: generate(p, 0),
        lambda p: generate(p, 1),
        lambda p: generate(p, 2),
    ],
    labels=["standard", "sym A", "sym B"],
    n_range=[n for n in range(4, 50, 4)],
    xlabel="Elements",
    # More optional arguments with their default values:
    # logx="auto",  # set to True or False to force scaling
    #logy=False,
    equality_check=None, #np.allclose,  # set to None to disable "correctness" assertion
    show_progress=True,
    # target_time_per_measurement=1.0,
    max_time=TIME_OUT,  # maximum time per measurement
    # time_unit="s",  # set to one of ("auto", "s", "ms", "us", or "ns") to force plot units
    # relative_to=1,  # plot the timings relative to one of the measurements
    # flops=lambda n: 3*n,  # FLOPS plots
    title=""
)
out.show()
out.save("symm.png", transparent=True, bbox_inches="tight")

Done = True