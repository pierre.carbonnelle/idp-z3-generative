x = input("""
Enter 0 for the Pigeonhole problem,
      1 for the Pigeon 2-2 problem,
      2 for the Pigeon 2-2 problem with minimization,
      3 for configuration problems (Z3)
      4 for Rack ABCD with symmetry breaking
      5 for configuration problem (CMPpy)
      6 for Golfer
      7 for Golfer 2
      8 for BAPA
> """)

if x == "0":
    from Pigeon import Done
elif x == "1":
    from Pigeon2 import Done
elif x == "2":
    from Pigeon2min import Done
elif x == "3":
    from perf import Done
elif x == "4":
    from symm import Done
elif x == "5":
    from CPMpy import Done
elif x == "6":
    from Golfer import Done
elif x == "7":
    from Golfer2 import Done
elif x == "8":
    from BAPA import Done
