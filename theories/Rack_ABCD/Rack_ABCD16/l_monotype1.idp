vocabulary V { 
    type ConfigObject 
    Rack: (ConfigObject) → 𝔹
    Frame: (ConfigObject) → 𝔹
    Module: (ConfigObject) → 𝔹
    Element: (ConfigObject) → 𝔹
    type RackType := {RackSingle, RackDouble}
    type ModuleType := {ModuleI, ModuleII, ModuleIII, ModuleIV, ModuleV}
    type ElementType := {ElementA, ElementB, ElementC, ElementD}
    module14: (Module) → 𝔹
    typeOfR: (Rack) → RackType
    typeOfM: (Module) → ModuleType
    typeOfE: (Element) → ElementType
    rackOfF: (Frame) → Rack
    frameOfM: (Module) → Frame
    elementOfM: (module14) → Element
    maxRack: () → ℤ
    maxFrame: () → ℤ
    maxModule: () → ℤ
    maxElement: () → ℤ
    mulConfigObject: (ConfigObject) → ℤ
    type TypeConfigObject := {Rack__, Frame__, Module__, Element__}
    typeConfigObject : ConfigObject -> TypeConfigObject
}
theory T:V { 
    [∀ r ∈ Rack: typeOfR(r) = RackSingle ⇒ #{f ∈ Frame : rackOfF(f) = r} = 4]
      ∀ r ∈ Rack: typeOfR(r) = RackSingle ⇒ sum{{ mulConfigObject(f) | f ∈ Frame : rackOfF(f) = r}} = 4 ⨯ mulConfigObject(r).
    [∀ r ∈ Rack: typeOfR(r) = RackDouble ⇒ #{f ∈ Frame : rackOfF(f) = r} = 8]
      ∀ r ∈ Rack: typeOfR(r) = RackDouble ⇒ sum{{ mulConfigObject(f) | f ∈ Frame : rackOfF(f) = r}} = 8 ⨯ mulConfigObject(r).
    [∀ f ∈ Frame: #{m ∈ Module : frameOfM(m) = f} ≤ 5]
      ∀ f ∈ Frame: sum{{ mulConfigObject(m) | m ∈ Module : frameOfM(m) = f}} ≤ 5 ⨯ mulConfigObject(f).
    [∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII) ⇔ #{m5 ∈ Module : frameOfM(m5) = f ∧ typeOfM(m5) = ModuleV} = 1]
      ∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII) ⇔ sum{{ mulConfigObject(m5) | m5 ∈ Module : frameOfM(m5) = f ∧ typeOfM(m5) = ModuleV}} = 1 ⨯ mulConfigObject(f).
    [∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleV) ⇒ (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII)]
      ∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleV) ⇒ (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII).
    [∀ e ∈ Element: typeOfE(e) = ElementA ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleI} = 1]
      ∀ e ∈ Element: typeOfE(e) = ElementA ⇒ sum{{ mulConfigObject(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleI}} = 1 ⨯ mulConfigObject(e).
    [∀ e ∈ Element: typeOfE(e) = ElementB ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleII} = 2]
      ∀ e ∈ Element: typeOfE(e) = ElementB ⇒ sum{{ mulConfigObject(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleII}} = 2 ⨯ mulConfigObject(e).
    [∀ e ∈ Element: typeOfE(e) = ElementC ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIII} = 3]
      ∀ e ∈ Element: typeOfE(e) = ElementC ⇒ sum{{ mulConfigObject(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIII}} = 3 ⨯ mulConfigObject(e).
    [∀ e ∈ Element: typeOfE(e) = ElementD ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIV} = 4]
      ∀ e ∈ Element: typeOfE(e) = ElementD ⇒ sum{{ mulConfigObject(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIV}} = 4 ⨯ mulConfigObject(e).
    [#{e ∈ Element : typeOfE(e) = ElementA} = 4]
      sum{{ mulConfigObject(e) | e ∈ Element : typeOfE(e) = ElementA}} = 4.
    [#{e ∈ Element : typeOfE(e) = ElementB} = 4]
      sum{{ mulConfigObject(e) | e ∈ Element : typeOfE(e) = ElementB}} = 4.
    [#{e ∈ Element : typeOfE(e) = ElementC} = 4]
      sum{{ mulConfigObject(e) | e ∈ Element : typeOfE(e) = ElementC}} = 4.
    [#{e ∈ Element : typeOfE(e) = ElementD} = 4]
      sum{{ mulConfigObject(e) | e ∈ Element : typeOfE(e) = ElementD}} = 4.
    ∀ mulConfigObject0_ ∈ ConfigObject: 0 ≤ mulConfigObject(mulConfigObject0_).
    ∀ f ∈ ConfigObject: ∃ n__5sldkqsdf ∈ ℤ: mulConfigObject(f) = n__5sldkqsdf ⨯ mulConfigObject(rackOfF(f)).
    ∀ m ∈ ConfigObject: ∃ n__5sldkqsdf ∈ ℤ: mulConfigObject(m) = n__5sldkqsdf ⨯ mulConfigObject(frameOfM(m)).
    ∀ m ∈ ConfigObject: ∃ n__5sldkqsdf ∈ ℤ: mulConfigObject(m) = n__5sldkqsdf ⨯ mulConfigObject(elementOfM(m)).
    { ∀ module140_ ∈ Module: module14(module140_) ← ¬(typeOfM(module140_) = ModuleV).
    }
    { !x in ConfigObject: typeConfigObject(x)=Rack__ <- Rack(x)& 0 < mulConfigObject(x).
      !x in ConfigObject: typeConfigObject(x)=Frame__ <- Frame(x)& 0 < mulConfigObject(x).
      !x in ConfigObject: typeConfigObject(x)=Module__ <- Module(x)& 0 < mulConfigObject(x).
      !x in ConfigObject: typeConfigObject(x)=Element__ <- Element(x)& 0 < mulConfigObject(x).
    }
    {
      !x in ConfigObject: typeConfigObject(x)=Rack__ <- 1 =< x < maxRack().
      !x in ConfigObject: typeConfigObject(x)=Frame__ <- maxRack() =< x < maxFrame().
      !x in ConfigObject: typeConfigObject(x)=Module__ <- maxFrame() =< x < maxModule().
      !x in ConfigObject: typeConfigObject(x)=Element__ <- maxModule() =< x < maxElement().
    }

    
}

procedure main() {
    logging.getLogger().setLevel(logging.INFO)
    interp, sol = Theory(T).generate(timeout_seconds=0, iter_seconds=0)
    pretty_print(sol)
    print(duration("Expansion"))
    print("-------------------")
    print(Theory(T).expand_lifting(sol))
    print("-------------------")
}
        