vocabulary V { 
    type Rack_T 
    Rack: (Rack_T) → 𝔹
    type Frame_T 
    Frame: (Frame_T) → 𝔹
    type Module_T 
    Module: (Module_T) → 𝔹
    type Element_T 
    Element: (Element_T) → 𝔹
    type RackType := {RackSingle, RackDouble}
    type ModuleType := {ModuleI, ModuleII, ModuleIII, ModuleIV, ModuleV}
    type ElementType := {ElementA, ElementB, ElementC, ElementD}
    module14: (Module) → 𝔹
    typeOfR: (Rack) → RackType
    typeOfM: (Module) → ModuleType
    typeOfE: (Element) → ElementType
    rackOfF: (Frame) → Rack
    frameOfM: (Module) → Frame
    elementOfM: (module14) → Element
    mulRack_T: (Rack_T) → ℤ
    mulFrame_T: (Frame_T) → ℤ
    mulModule_T: (Module_T) → ℤ
    mulElement_T: (Element_T) → ℤ
}
theory T:V { 
    [∀ r ∈ Rack: typeOfR(r) = RackSingle ⇒ #{f ∈ Frame : rackOfF(f) = r} = 4]
      ∀ r ∈ Rack: typeOfR(r) = RackSingle ⇒ sum{{ mulFrame_T(f) | f ∈ Frame : rackOfF(f) = r}} = 4 ⨯ mulRack_T(r).
    [∀ r ∈ Rack: typeOfR(r) = RackDouble ⇒ #{f ∈ Frame : rackOfF(f) = r} = 8]
      ∀ r ∈ Rack: typeOfR(r) = RackDouble ⇒ sum{{ mulFrame_T(f) | f ∈ Frame : rackOfF(f) = r}} = 8 ⨯ mulRack_T(r).
    [∀ f ∈ Frame: #{m ∈ Module : frameOfM(m) = f} ≤ 5]
      ∀ f ∈ Frame: sum{{ mulModule_T(m) | m ∈ Module : frameOfM(m) = f}} ≤ 5 ⨯ mulFrame_T(f).
    [∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII) ⇔ #{m5 ∈ Module : frameOfM(m5) = f ∧ typeOfM(m5) = ModuleV} = 1]
      ∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII) ⇔ sum{{ mulModule_T(m5) | m5 ∈ Module : frameOfM(m5) = f ∧ typeOfM(m5) = ModuleV}} = 1 ⨯ mulFrame_T(f).
    [∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleV) ⇒ (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII)]
      ∀ f ∈ Frame: (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleV) ⇒ (∃ m2 ∈ Module: frameOfM(m2) = f ∧ typeOfM(m2) = ModuleII).
    [∀ e ∈ Element: typeOfE(e) = ElementA ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleI} = 1]
      ∀ e ∈ Element: typeOfE(e) = ElementA ⇒ sum{{ mulModule_T(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleI}} = 1 ⨯ mulElement_T(e).
    [∀ e ∈ Element: typeOfE(e) = ElementB ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleII} = 2]
      ∀ e ∈ Element: typeOfE(e) = ElementB ⇒ sum{{ mulModule_T(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleII}} = 2 ⨯ mulElement_T(e).
    [∀ e ∈ Element: typeOfE(e) = ElementC ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIII} = 3]
      ∀ e ∈ Element: typeOfE(e) = ElementC ⇒ sum{{ mulModule_T(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIII}} = 3 ⨯ mulElement_T(e).
    [∀ e ∈ Element: typeOfE(e) = ElementD ⇒ #{m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIV} = 4]
      ∀ e ∈ Element: typeOfE(e) = ElementD ⇒ sum{{ mulModule_T(m) | m ∈ Module : elementOfM(m) = e ∧ typeOfM(m) = ModuleIV}} = 4 ⨯ mulElement_T(e).
    [#{e ∈ Element : typeOfE(e) = ElementA} = 1]
      sum{{ mulElement_T(e) | e ∈ Element : typeOfE(e) = ElementA}} = 1.
    [#{e ∈ Element : typeOfE(e) = ElementB} = 1]
      sum{{ mulElement_T(e) | e ∈ Element : typeOfE(e) = ElementB}} = 1.
    [#{e ∈ Element : typeOfE(e) = ElementC} = 1]
      sum{{ mulElement_T(e) | e ∈ Element : typeOfE(e) = ElementC}} = 1.
    [#{e ∈ Element : typeOfE(e) = ElementD} = 1]
      sum{{ mulElement_T(e) | e ∈ Element : typeOfE(e) = ElementD}} = 1.
    ∀ mulRack_T0_ ∈ Rack_T: 0 ≤ mulRack_T(mulRack_T0_).
    ∀ mulFrame_T0_ ∈ Frame_T: 0 ≤ mulFrame_T(mulFrame_T0_).
    ∀ mulModule_T0_ ∈ Module_T: 0 ≤ mulModule_T(mulModule_T0_).
    ∀ mulElement_T0_ ∈ Element_T: 0 ≤ mulElement_T(mulElement_T0_).
    ∀ f ∈ Frame_T: ∃ n__5sldkqsdf ∈ ℤ: mulFrame_T(f) = n__5sldkqsdf ⨯ mulRack_T(rackOfF(f)).
    ∀ m ∈ Module_T: ∃ n__5sldkqsdf ∈ ℤ: mulModule_T(m) = n__5sldkqsdf ⨯ mulFrame_T(frameOfM(m)).
    ∀ m ∈ Module_T: ∃ n__5sldkqsdf ∈ ℤ: mulModule_T(m) = n__5sldkqsdf ⨯ mulElement_T(elementOfM(m)).
    { ∀ module140_ ∈ Module: module14(module140_) ← ¬(typeOfM(module140_) = ModuleV).
    }
    ! x in Rack_T: Rack(x) <=> 0 < mulRack_T(x).
    ! x in Frame_T: Frame(x) <=> 0 < mulFrame_T(x).
    ! x in Module_T: Module(x) <=> 0 < mulModule_T(x).
    ! x in Element_T: Element(x) <=> 0 < mulElement_T(x).
    
}

procedure main() {
    logging.getLogger().setLevel(logging.INFO)
    interp, sol = Theory(T).generate(factor=1.5, timeout_seconds=0, iter_seconds=0)
    pretty_print(sol)
    print(duration("Expansion"))
    print("-------------------")
    print(Theory(T).expand_lifting(sol))
    print("-------------------")
}
        